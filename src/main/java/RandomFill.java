import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

class RandomFill {

    static ArrayList<Shape> fill() {
        ArrayList<Shape> shapes = new ArrayList<>();
        Shape obj = null;
        Random rnd = new Random();
        for (int i = 0; i < randInt(); i++) {
            int randomNumber = rnd.nextInt(4);
            switch(randomNumber) {
                case 0:
                    obj = new Circle();
                    ((Circle) obj).setRadius(randInt());
                    ((Circle) obj).setColor(randomColor());
                    obj.calculateArea();
                    break;
                case 1:
                    obj = new Square();
                    ((Square) obj).setSizeA(randInt());
                    ((Square) obj).setColor(randomColor());
                    obj.calculateArea();
                    break;
                case 2:
                    obj = new Triangle();
                    ((Triangle) obj).setSizeA(randInt());
                    ((Triangle) obj).setSizeB(randInt());
                    ((Triangle) obj).setSizeC(randInt());
                    ((Triangle) obj).setColor(randomColor());
                    obj.calculateArea();
                    break;
                case 3:
                    obj = new Trapezium();
                    ((Trapezium) obj).setSizeB(randInt());
                    ((Trapezium) obj).setSizeC(randInt());
                    ((Trapezium) obj).setSizeH(randInt());
                    ((Trapezium) obj).setColor(randomColor());
                    obj.calculateArea();
                    break;
            }
            shapes.add(obj);
        }
        return shapes;
    }

    private static double randInt() {
        Random rnd = new Random();
        return rnd.nextInt(100);
    }

    private static String randomColor() {
        Random rnd = new Random();
        Color colors[] = { Color.BLUE, Color.GREEN, Color.MAGENTA, Color.RED, Color.CYAN };
        int number = rnd.nextInt(colors.length);
        if(number == 0)
            return "Blue";
        else if(number == 1)
            return "Green";
        else if(number == 2)
            return "Magenta";
        else if(number == 3)
            return "Red";
        else if(number == 4)
            return "Cyan";
        else
            return "Darkness";
    }
}
