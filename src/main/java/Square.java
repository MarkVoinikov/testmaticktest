public class Square implements Shape {
    private double sizeA =0;
    private String color;


    void setSizeA(double a) {
        sizeA = a;
    }

    double getSizeA() {
        return sizeA;
    }

    void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public double calculateArea() {
        return 2 * sizeA;
    }

    public String toString() {
        return "Фигура: квадрат, площадь: " + calculateArea() + " кв. ед., длина стороны: " + getSizeA()
                + " ед., цвет: " + getColor();
    }


}
