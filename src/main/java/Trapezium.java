public class Trapezium implements Shape {
    private static double sizeB = 0;
    private static double sizeC = 0;
    private static double sizeH = 0;
    private String color;

    void setSizeB(double b) {
        sizeB = b;
    }

    void setSizeC(double c) {
        sizeC = c;
    }

    void setSizeH(double h) {
        sizeH = h;
    }

    void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public double calculateArea() {
        return (0.5) * ((sizeB + sizeC) * sizeH);
    }

    public String toString() {
        return "Фигура: трапеция, площадь: " + calculateArea() + " кв. ед., высота: " + sizeH + "ед., цвет: " + getColor();
    }
}
