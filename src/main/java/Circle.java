public class Circle implements Shape {
    private static double radius = 0;
    private String color;

    void setRadius(double a) {
        radius = a;
    }

    static double getRadius() {
        return radius;
    }

    void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public double calculateArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public String toString() {
        return "Фигура: круг, площадь: " + calculateArea() + " кв. ед., радиус: " + getRadius() + " ед., цвет: " + getColor();
    }
}
