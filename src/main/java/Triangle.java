public class Triangle implements Shape {
    private static double sizeA = 0;
    private static double sizeB = 0;
    private static double sizeC = 0;
    private String color;

    void setSizeA(double a) {
        sizeA = a;
    }

    void setSizeB(double b) {
        sizeB = b;
    }

    void setSizeC(double c) {
        sizeC = c;
    }

    double getSizeC() {
        return sizeC;
    }

    void setColor(String color) {
        this.color = color;
    }

    private double calculatePerimeter() {
        return sizeA + sizeB + sizeC;
    }


    public String getColor() {
        return color;
    }

    public double calculateArea() {
        double p = calculatePerimeter() / 2;
        return Math.sqrt(p * (p - sizeA) * (p - sizeB) * (p - sizeC));
    }

    public String toString() {
        return "Фигура: триугольник, площадь: " + calculateArea() + " кв. ед., длина гипотенузы: " + getSizeC()
                + " ед., цвет: " + getColor();
    }
}
