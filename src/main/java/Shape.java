interface Shape {

    String getColor();

    double calculateArea();

}
